import { createApp } from 'vue'
import {createRouter, createWebHashHistory} from 'vue-router'

import App from './App.vue'

import LoginCmt from './components/pages/LoginCmt'
import RecoverPssCmt from './components/pages/RecoverPssCmt'

import LayoutCmt from './components/containers/LayoutCmt'
//import SiderbarCmt from './components/containers/SiderbarCmt'

import UsuariosCmt from './components/views/UsuariosCmt'

const routes = [
    {path:'/login', component: LoginCmt},
    {path:'/recover', component: RecoverPssCmt},
    {
        path:'/', 
        component: LayoutCmt,
        children:[
            {path:'usuario', component: UsuariosCmt}
        ]
    }

]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

const app = createApp(App)
app.use(router)
app.mount('#app')

